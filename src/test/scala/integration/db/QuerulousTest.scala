package integration.db

import org.scalatest.{FlatSpec, BeforeAndAfterAll}
import sample.Application
import com.twitter.querulous.evaluator.Transaction
import java.sql.ResultSet

class QuerulousTest extends FlatSpec with BeforeAndAfterAll {

  override def convertToEqualizer(left: Any) = new Equalizer(left)

  /** Method to replace assert(value === expected) since === is used by Squeryl */
  def assertEquals[T](value:T, expected:T) {
    assert(convertToEqualizer(value) === expected)
  }

  behavior of "Squeryl"

  def rsToInt = {rs:ResultSet => rs.getInt(1)}
  def rsToString = {rs:ResultSet => rs.getString(1)}

  override protected def beforeAll() {
    Application.init()
    Application.queryEvaluator.transaction { tx:Transaction =>
      val idMartin = tx.selectOne[Int]("insert into author(first_name,last_name) values (?,?) returning id","Martin","Odersky")(rsToInt)
      tx.execute("insert into book(title,author_id) values (?,?)","Programming in Scala",idMartin.get)
    }
  }


  it should "execute simple queries" in {
    Application.queryEvaluator.transaction { tx:Transaction =>
      val name = tx.selectOne[String]("select first_name from author a where a.first_name = 'Martin'")(rsToString)
      assertEquals(name.get,"Martin")
    }
  }

}
