-- table declarations :
create table author (
    email varchar(128),
    last_name varchar(128) not null,
    first_name varchar(128) not null,
    id serial primary key
  );
create sequence s_author_id;
-- indexes on Author
create unique index "idxEmailAddresses" on author (email);
create index "idxLastName" on author (last_name);
create index "idxFirstName" on author (first_name);
create table book (
    id serial primary key,
    author_id bigint not null,
    title varchar(128) not null
  );
create sequence s_book_id;
-- column group indexes :
create index "idxName" on author (first_name,last_name);