package sample

import com.mchange.v2.c3p0.{DriverManagerDataSource, ComboPooledDataSource}
import com.googlecode.flyway.core.Flyway
import com.twitter.querulous.evaluator._
import com.twitter.querulous.query._
import com.twitter.querulous.database._
import com.twitter.conversions.time._

object Application {

  val databaseUrl = "jdbc:postgresql://localhost/test"

  val databaseUsername = "test"

  val databasePassword = "test"

  val databaseDriverName = "org.postgresql.Driver"

  val databaseDriverClass = Class.forName(databaseDriverName)

  val dataSource = {
    val ds = new DriverManagerDataSource()
    ds.setDriverClass(databaseDriverName)
    ds.setJdbcUrl(databaseUrl)
    ds.setUser(databaseUsername)
    ds.setPassword(databasePassword)
    ds
  }

  val queryFactory = new SqlQueryFactory

  val apachePoolingDatabaseFactory = new ApachePoolingDatabaseFactory(
    minOpenConnections=1,
    maxOpenConnections=5,
    checkConnectionHealthWhenIdleFor = 5.seconds,
    maxWaitForConnectionReservation = 1.seconds,
    checkConnectionHealthOnReservation = false,
    evictConnectionIfIdleFor=15.seconds,
    transactionIsolation=None,
    defaultUrlOptions=Map())

  val queryEvaluatorFactory = new StandardQueryEvaluatorFactory(apachePoolingDatabaseFactory, queryFactory)

  val queryEvaluator = queryEvaluatorFactory(dbhost ="localhost", dbname="test", username=databaseUsername, password=databasePassword, urlOptions=Map(), driverName="jdbc:postgresql")

  val migrations = {
    val res = new Flyway()
    res.setDataSource(dataSource)
    res
  }

  def init() {
    //migrations.clean()
    migrations.migrate()
  }

}
