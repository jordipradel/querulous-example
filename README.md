# Querulous example

This is another sample project on how to use relational databases in Scala. After my first attempt with Squeryl, I’ve decided to try Querulous. The main motivation behind this is to be able to use plain old SQL as the best DSL for relational database querying and DDL. The second motivation is Twitter is behind Querulous.

In this example I’ll be using:

* [Flyway](http://flywaydb.org/) 2.0 for database migrations based in plain old SQL
* [PostgreSQL](http://www.postgresql.org/) 9.2.1 as the relational DB
* [C3P0](http://www.mchange.com/projects/c3p0/) 0.9.1.2 as the database pool, which I need just to show how to use such a pool with Squeryl
* Scala 2.9.2, since it is the latest stable version (at the time of writing)
* [Scalatest](http://www.scalatest.org/) 1.8 for unit and integration testing


## Why Flyway

I was already using Flyway and it has been working flawlessly. So, check my motivations in my other project.