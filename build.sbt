name := "Squeryl Scala example"

version := "0.1"

scalaVersion := "2.9.2"

resolvers += "Twitter Repository" at "http://maven.twttr.com/"

libraryDependencies ++= Seq(
  "com.twitter" % "querulous-core" % "3.0.3",
  "postgresql" % "postgresql" % "8.4-701.jdbc4",
  "c3p0" % "c3p0" % "0.9.1.2",
  "com.googlecode.flyway" % "flyway-core" % "2.0",
  "org.scalatest" %% "scalatest" % "1.8" % "test"
)

seq(cucumberSettings : _*)
